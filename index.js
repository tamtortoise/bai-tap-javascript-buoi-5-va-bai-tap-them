// Bài 1: Quản lý tuyển sinh

// Biến khu vực + biến đối tượng -> hàm áp dụng khu vực -> hàm đối tượng => Hàm tổng hợp (gồm hàm khu vực/đối tượng và hàm đối tượng)

// Tạo biến khu vực và đối tượng
const VUNG_CAO = "vungCao";
const VUNG_XA = "vungXa";
const VUNG_KHO_KHAN = "vungKhoKhan";
const NGHEO_BEN_VUNG = "ngheoBenVung";
const MO_COI = "moCoi";
const KHUYET_TAT = "khuyetTat";

// hàm áp dụng điểm ưu tiên theo khu vực
function HSUuTienTheoKhuVuc(uutien) {
  var diemUuTien;
  if (uutien == VUNG_CAO) {
    diemUuTien = 2;
  } else if (uutien == VUNG_XA) {
    diemUuTien = 1;
  } else if (uutien == VUNG_KHO_KHAN) {
    diemUuTien = 0.5;
  } else {
    diemUuTien = 0;
  }
  return diemUuTien;
}

// hàm áp dụng điểm ưu tiên theo đối tượng
function HSoUuTienTheoDoiTuong(uutien) {
  if (uutien == NGHEO_BEN_VUNG) {
    diemUuTien = 2.5;
  } else if (uutien == MO_COI) {
    diemUuTien = 1.5;
  } else if (uutien == KHUYET_TAT) {
    diemUuTien = 1;
  } else {
    diemUuTien = 0;
  }
  return diemUuTien;
}
// Hàm mì trộn ra kết quả
function xetTuyen() {
  var diemMon1 = document.getElementById("txt-toan").value * 1;
  var diemMon2 = document.getElementById("txt-van").value * 1;
  var diemMon3 = document.getElementById("txt-anh").value * 1;
  var diemChuan = document.getElementById("txt-chuan").value * 1;
  var khuVuc = document.getElementById("txt-area").value;
  var doiTuong = document.getElementById("txt-doituong").value;
  var diemUuTienKhuVuc = HSUuTienTheoKhuVuc(khuVuc);
  var diemUuTienDoiTuong = HSoUuTienTheoDoiTuong(doiTuong);
  var diemTong = 0;
  var diemTongGomUuTien = 0;
  diemTong = diemMon1 + diemMon2 + diemMon3;
  diemTongGomUuTien = diemTong + diemUuTienKhuVuc + diemUuTienDoiTuong;

  if (diemTongGomUuTien < diemChuan) {
    document.getElementById("result1").innerHTML = `Bạn đã lọt sàn. Tổng điểm là ${diemTongGomUuTien} thấp hơn điểm chuẩn yêu cầu`;
  } else {
    document.getElementById("result1").innerHTML = `Chúc mừng, bạn đã đậu. Tổng điểm là ${diemTongGomUuTien} (điểm chính: ${diemTong} + điểm ưu tiên: ${
      diemUuTienKhuVuc + diemUuTienDoiTuong
    })`;
  }
}


// Bài 2: Tính tiền điện

//hàm tính tiền điện theo Kw tiêu thụ
function tienDienTheoKWTieuThu(tieuthu) {
  var tienSoKWTieuThu;
  if (tieuthu <= 50) {
    tienSoKWTieuThu = 500 * tieuthu;
  } else if (tieuthu <= 100) {
    tienSoKWTieuThu = 650 * (tieuthu - 50) + 500 * 50;
  } else if (tieuthu <= 200) {
    tienSoKWTieuThu = 850 * (tieuthu - 100) + 650 * 50 + 500 * 50;
  } else if (tieuthu <= 350) {
    tienSoKWTieuThu = 1100 * (tieuthu - 200) + 850 * 100 + 650 * 50 + 500 * 50;
  } else {
    tienSoKWTieuThu =
      1300 * (tieuthu - 350) + 1100 * 150 + 850 * 100 + 650 * 50 + 500 * 50;
  }
  return tienSoKWTieuThu;
}

// hàm tính tiền

function tienDien() {
  var hoTen = document.getElementById("txt-hoTen").value;
  var soKW = document.getElementById("txt-soKW").value;
  var tienDienPhaiTra = tienDienTheoKWTieuThu(soKW);
  document.getElementById("result2").innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${tienDienPhaiTra.toLocaleString()} VND`;
}


// Bài 3: Tính thuế thu nhập cá nhân

// tạo hàm tính thuế thu nhập

function thuNhapChiuThue(thunhap) {
  var thueThuNhap;
  if (thunhap <= 10000000) {
    thueThuNhap = 0;
  } else if (thunhap <= 60000000) {
    thueThuNhap = 0.05;
  } else if (thunhap <= 120000000) {
    thueThuNhap = 0.1;
  } else if (thunhap <= 210000000) {
    thueThuNhap = 0.15;
  } else if (thunhap <= 384) {
    thueThuNhap = 0.2;
  } else if (thunhap <= 624000000) {
    thueThuNhap = 0.25;
  } else if (thunhap <= 960000000) {
    thueThuNhap = 0.3;
  } else {
    thueThuNhap = 0.35;
  }
  return thueThuNhap;
}

// Hàm tính thuế

function tienThue() {
  const GIAM_CO_DINH = 4000000;
  var hoTen = document.getElementById("txt-hoTen").value;
  var tongThuNhapNam = document.getElementById("txt-thunhap").value * 1;
  var soNguoiPhuThuoc = document.getElementById("txt-phuthuoc").value * 1;
  var phanTramThue = thuNhapChiuThue(tongThuNhapNam);

  var tongTienThueThuNhapCaNhan = 0;
  tongTienThueThuNhapCaNhan =
    (tongThuNhapNam - GIAM_CO_DINH - soNguoiPhuThuoc * 1600000) * phanTramThue;

  // kết quả

  document.getElementById("result3").innerHTML = `Họ tên: ${hoTen}; Tiền thuế thu nhập cá nhân: ${tongTienThueThuNhapCaNhan.toLocaleString()} vnđ`;

  // miễn thuế cho trường hợp thu nhập năm quá thấp

  if (tongThuNhapNam <= 10000000) {
    document.getElementById("result3").innerHTML = `Miễn thuế`;
  }
}


// Bài 4: tính tiền cáp


const Ca_nhan = "caNhan";
const DN = "doanhNghiep";

// tạo hàm tính chi phí

function chiPhiLoaiKhachHang(loaiKH) {

  var chiPhi = 0;
  var phiHoaDonND = 4.5;
  var phiHoaDonDN = 15;
  var phiDVCBND = 20.5;
  var phiDVCBDN = 75;
  var soKenhCC = document.getElementById("txt-kenh").value * 1;
  var phiKenhCCND = 7.5;
  var phiKenhCCDN = 50;
  var soKetNoiDN = document.getElementById("txt-ketnoi").value * 1;

  if (loaiKH == DN) {
    if (soKetNoiDN <= 10) {
      chiPhi = phiHoaDonDN + phiDVCBDN + (soKenhCC * phiKenhCCDN);
    } else {
      chiPhi =
        phiHoaDonDN + (phiDVCBDN + 5 * (soKetNoiDN - 10)) + (soKenhCC * phiKenhCCDN);
    }
    return chiPhi;
  }
  if (loaiKH == Ca_nhan) {
    chiPhi = phiHoaDonND + phiDVCBND + (soKenhCC * phiKenhCCND);
  } else {
    chiPhi = 0;
  }
  return chiPhi;
}

// hàm tính tiền
function tienCap() {
  var maKH = document.getElementById("txt-code").value;
  var loaiKH = document.getElementById("typeKH").value;
  var tienCapFinal = chiPhiLoaiKhachHang(loaiKH);
  document.getElementById("result4").innerHTML = `Mã khách hàng: ${maKH}; Tiền cáp: $${tienCapFinal}`;
}

// ẩn hiện tab 
function chonKH() {

  let KH_type = document.getElementById("typeKH").value;

  if (KH_type = DN) {
    document.getElementById("txt-ketnoi").style.visibility = "visible";
  } 
  
  else {
    document.getElementById("txt-ketnoi").style.visibility = "hidden";
  }
    return KH_type;
}




    



   
    